-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.17-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para tbl_improving
DROP DATABASE IF EXISTS `tbl_improving`;
CREATE DATABASE IF NOT EXISTS `tbl_improving` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tbl_improving`;

-- Copiando estrutura para tabela tbl_improving.cliente
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `Id_Cliente` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Cliente` varchar(50) DEFAULT NULL,
  `Data_Entrada` varchar(11) DEFAULT NULL,
  `Data_Saida` varchar(11) DEFAULT NULL,
  `Endereco_Cliente` varchar(50) DEFAULT NULL,
  `Cidade` varchar(50) DEFAULT NULL,
  `Estado` varchar(30) DEFAULT NULL,
  `Cep` varchar(11) DEFAULT NULL,
  `Cpf` varchar(14) DEFAULT NULL,
  `Rg` varchar(14) DEFAULT NULL,
  `Telefone_Residencial` varchar(14) DEFAULT NULL,
  `Celular_Contato` varchar(14) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  `Facebook` varchar(40) DEFAULT NULL,
  `Twitter` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`Id_Cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela tbl_improving.funcionario
DROP TABLE IF EXISTS `funcionario`;
CREATE TABLE IF NOT EXISTS `funcionario` (
  `Id_Fun` int(11) NOT NULL AUTO_INCREMENT,
  `Fun_Nome` varchar(50) DEFAULT NULL,
  `Fun_Endereco` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_Fun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela tbl_improving.itens_produtos_venda
DROP TABLE IF EXISTS `itens_produtos_venda`;
CREATE TABLE IF NOT EXISTS `itens_produtos_venda` (
  `Id_Venda_Itens` int(11) DEFAULT NULL,
  `Id_Produto_Itens` int(11) DEFAULT NULL,
  `Quantidade_Itens` int(11) DEFAULT NULL,
  `Id_Cliente` int(11) DEFAULT NULL,
  `Id_Produto` int(11) DEFAULT NULL,
  `Id_Venda` int(11) DEFAULT NULL,
  KEY `Id_Produto` (`Id_Produto`),
  KEY `Id_Venda` (`Id_Venda`),
  CONSTRAINT `Id_Produto` FOREIGN KEY (`Id_Produto`) REFERENCES `produto` (`Id_Produto`) ON DELETE NO ACTION,
  CONSTRAINT `Id_Venda` FOREIGN KEY (`Id_Venda`) REFERENCES `venda` (`Id_Venda`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela tbl_improving.produto
DROP TABLE IF EXISTS `produto`;
CREATE TABLE IF NOT EXISTS `produto` (
  `Id_Produto` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Produto` varchar(50) DEFAULT NULL,
  `Data_Pedido` varchar(11) DEFAULT NULL,
  `Data_Entrega` varchar(11) DEFAULT NULL,
  `Quantidade` varchar(10) DEFAULT NULL,
  `Descricao_Produto` varchar(150) DEFAULT NULL,
  `Preco_Custo` varchar(10) DEFAULT NULL,
  `Preco_Venda` varchar(10) DEFAULT NULL,
  `Codigo_Barra` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`Id_Produto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela tbl_improving.venda
DROP TABLE IF EXISTS `venda`;
CREATE TABLE IF NOT EXISTS `venda` (
  `Id_Venda` int(11) NOT NULL AUTO_INCREMENT,
  `Data_Venda` varchar(11) DEFAULT NULL,
  `Valor_Venda` decimal(7,2) DEFAULT NULL,
  `Id_Cli` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_Venda`),
  KEY `Id_Cli` (`Id_Cli`),
  CONSTRAINT `Id_Cli` FOREIGN KEY (`Id_Cli`) REFERENCES `cliente` (`Id_Cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
