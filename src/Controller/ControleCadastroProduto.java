
package Controller;


import Dao.ClassConectaBanco;
import Model.ModeloCadastroProduto;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author: José Walter.
 */
public class ControleCadastroProduto {
    ClassConectaBanco conn = new ClassConectaBanco();
    ModeloCadastroProduto mod = new ModeloCadastroProduto();
           
//-------------------------------------------------------------------------------------------------------------------------    
    
    public void InserirProduto(ModeloCadastroProduto mod){
        
        conn.conexao();
        try {
            PreparedStatement pst = conn.conn.prepareStatement("INSERT INTO produto ("
            + "Nome_Produto, Data_Pedido, Data_Entrega, Quantidade, Descricao_Produto, Preco_Custo, Preco_Venda,"
            + "Codigo_Barra) VALUES (?,?,?,?,?,?,?,?)");
            pst.setString(1, mod.getNomeProduto());
            pst.setString(2, mod.getDataPedido());
            pst.setString(3, mod.getDataEntrega());
            pst.setString(4, mod.getQuantidade());
            pst.setString(5, mod.getDescricaoProduto());
            pst.setString(6, mod.getPrecoCusto());
            pst.setString(7, mod.getPrecoVenda());
            pst.setString(8, mod.getCodigoBarra());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Produto inserido com sucesso!!");
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Erro ao inserir o produto!!\nErro" +ex);
        }
        conn.desconecta();
    }
 
//------------------------------------------------------------------------------------------------------------------------- 

public void EditarProduto(ModeloCadastroProduto mod){
   
    conn.conexao();
     
        try {
            PreparedStatement pst = conn.conn.prepareStatement("UPDATE produto SET "
            + " Nome_Produto =?, Data_Pedido =?, Data_Entrega =?, Quantidade =?, Descricao_Produto =?,"
            + " Preco_Custo =?, Preco_Venda =?, Codigo_Barra =? WHERE Id_Produto =?");
            pst.setString(1, mod.getNomeProduto());
            pst.setString(2, mod.getDataPedido());
            pst.setString(3, mod.getDataEntrega());
            pst.setString(4, mod.getQuantidade());
            pst.setString(5, mod.getDescricaoProduto());
            pst.setString(6, mod.getPrecoCusto());
            pst.setString(7, mod.getPrecoVenda());
            pst.setString(8, mod.getCodigoBarra());
            pst.setInt(9, mod.getCodigoProduto());
            pst.execute();  
            JOptionPane.showMessageDialog(null, "Produto alterado com sucesso!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterr o produto!!\nErro" +ex);
        }
conn.desconecta();

}  

//--------------------------------------------------------------------------------------------------------------------------------
      public void ExcluirProduto(ModeloCadastroProduto mod){
          conn.conexao();
          
        try {
            PreparedStatement pst = conn.conn.prepareStatement("DELETE FROM produto WHERE Id_Produto =? ");
            pst.setInt(1, mod.getCodigoProduto());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Produto excluido com sucesso!!");
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Erro ao excluir o produto!!\nErro" +ex);
        }         
          conn.desconecta();                
      }      
    
//--------------------------------------------------------------------------------------------------------------------------------

   public ModeloCadastroProduto BuscaProduto(ModeloCadastroProduto mod){
      conn.conexao();
        try {
            conn.executaSQL("SELECT * FROM produto WHERE Nome_Produto like  '%" + mod.getPesquisar() +"%'");
            conn.rs.first();
            mod.setCodigoProduto(conn.rs.getInt("Id_Produto"));
            mod.setNomeProduto(conn.rs.getString("Nome_Produto"));
            mod.setDataPedido(conn.rs.getString("Data_Pedido"));
            mod.setDataEntrega(conn.rs.getString("Data_Entrega"));
            mod.setQuantidade(conn.rs.getString( "Quantidade"));
            mod.setDescricaoProduto(conn.rs.getString("Descricao_Produto"));
            mod.setPrecoCusto(conn.rs.getString("Preco_Custo"));
            mod.setPrecoVenda(conn.rs.getString("Preco_Venda"));
            mod.setCodigoBarra(conn.rs.getString("Codigo_Barra"));
                       
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao buscar o produto!!\nErro" +ex);
        }
      conn.desconecta();
      return mod;
   
   }    

}
