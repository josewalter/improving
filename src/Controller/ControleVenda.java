
package Controller;


import Dao.ClassConectaBanco;
import Model.ModeloVenda;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author: José Walter.
 */
public class ControleVenda {
    ClassConectaBanco connvenda = new ClassConectaBanco();
    
    int codProduto;  
      
    
    //------------------------------------------------------------------------------------------------------------------
    
    public void AdicionaItem(ModeloVenda mod){
        
        AchaProduto(mod.getNomeProduto());
        connvenda.conexao();
          try {
            PreparedStatement pst = connvenda.conn.prepareStatement("INSERT INTO itens_produtos_venda "
            + "(Id_Venda, Id_Produto, Quantidade_Itens) VALUES (?,?,?) ");
            pst.setInt(1, mod.getIdvenda());
            pst.setInt(2, codProduto);
            pst.setInt(3, mod.getQuantidadeItem());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Produto adicionado com sucesso !!");
          //Baixa no estoque.
            int quantidade = 0, result = 0;
              try {
                  connvenda.executaSQL("SELECT * FROM produto WHERE Nome_Produto='"+ mod.getNomeProduto()+"'");
                  connvenda.rs.first();
                  quantidade = connvenda.rs.getInt("Quantidade");
                  result = quantidade - mod.getQuantidadeItem();
                  pst = connvenda.conn.prepareStatement("UPDATE produto SET Quantidade = ? WHERE Nome_Produto = ?");
                  pst.setInt(1, result);
                  pst.setString(2, mod.getNomeProduto());
                  pst.execute();
                  JOptionPane.showMessageDialog(null, "Baixa do produto realizada com sucesso !!");
              } catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "Erro ao realizar a baixa do  produto!!\nErro" + ex);
              }
              connvenda.desconecta();
          } catch (SQLException ex) {
              connvenda.desconecta();      
              JOptionPane.showMessageDialog(null, "Erro ao adicionar o  produto!!\nErro" + ex);
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------
    
     public void AchaProduto(String nome){
         connvenda.conexao();
        try {
            connvenda.executaSQL("SELECT * FROM produto WHERE Nome_Produto ='" + nome +"'");
            connvenda.rs.first();
            codProduto = connvenda.rs.getInt("Id_Produto");
            connvenda.desconecta();
        } catch (Exception ex) {
            connvenda.desconecta();
            JOptionPane.showMessageDialog(null, "Erro ao achar o produto !!\nErro" + ex);
        }
     }     
//-----------------------------------------------------------------------------------------------------------------

   public void FechaVenda(ModeloVenda mod){
       
       AchaCliente(mod.getNomeCliente());
       connvenda.conexao();
        try {
            PreparedStatement pst = connvenda.conn.prepareStatement("UPDATE venda SET "
            + "Data_Venda = ?, Valor_Venda = ?, Id_Cliente = ? WHERE Id_Venda = ?");
            pst.setString(1, mod.getDataVenda());
            pst.setFloat(2, mod.getValorVenda());
           int codCliente = 0;
            pst.setInt(3, codCliente);
            pst.setInt(4, mod.getIdvenda());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Venda finalizada com sucesso !!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao fechar a venda!\nErro:" + ex);
        }
        
       connvenda.desconecta();
   }

//-----------------------------------------------------------------------------------------------------------------
   
   public void AchaCliente(String nome){
       
       connvenda.conexao();
        
        try {
            connvenda.executaSQL("SELECT * FROM cliente WHERE Nome_Cliente = '" + nome + "'");
            connvenda.rs.first();
           int codCliente = connvenda.rs.getInt("Id_Cliente");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao achar o cliente!\nErro:" + ex);
        }       
       connvenda.desconecta();
   
   }   
 }
