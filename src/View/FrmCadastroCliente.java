
package View;



import Controller.ControleCadastroCliente;
import Dao.ClassConectaBanco;
import Model.ModeloCadastroCliente;
import Model.ModeloTabela;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author: José Walter.
 */
public class FrmCadastroCliente extends javax.swing.JFrame {
     ClassConectaBanco conectar = new  ClassConectaBanco();
     ModeloCadastroCliente mod = new ModeloCadastroCliente();
     
    public FrmCadastroCliente() {
        initComponents();
        conectar.conexao();
        
 
         preencherTabela("SELECT * FROM cliente ORDER BY Id_Cliente");
        }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanelPrincipal = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableCliente = new javax.swing.JTable();
        jPanelButon = new javax.swing.JPanel();
        jButtonNovo = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonFechar = new javax.swing.JButton();
        jButtonPrimeiro = new javax.swing.JButton();
        jButtonProximo = new javax.swing.JButton();
        jButtonAnterior = new javax.swing.JButton();
        jButtonUltimo = new javax.swing.JButton();
        jLabelIdCliente = new javax.swing.JLabel();
        jTextFieldIdCliente = new javax.swing.JTextField();
        jTextFieldNomeCliente = new javax.swing.JTextField();
        jLabelNomeCliente = new javax.swing.JLabel();
        jFormattedTextFieldDataEntrada = new javax.swing.JFormattedTextField();
        jLabelDataEntrada = new javax.swing.JLabel();
        jFormattedTextFieldDataSaida = new javax.swing.JFormattedTextField();
        jLabelDataSaida = new javax.swing.JLabel();
        jTextFieldEndereco = new javax.swing.JTextField();
        jLabelEndereco = new javax.swing.JLabel();
        jTextFieldCidade = new javax.swing.JTextField();
        jLabelCidade = new javax.swing.JLabel();
        jLabelCep = new javax.swing.JLabel();
        jLabelTelefoneResidencial = new javax.swing.JLabel();
        jLabelCelularContato = new javax.swing.JLabel();
        jFormattedTextFieldCelularContato = new javax.swing.JFormattedTextField();
        jLabelCpf = new javax.swing.JLabel();
        jLabelRg = new javax.swing.JLabel();
        jTextFieldEmail = new javax.swing.JTextField();
        jLabelEmail = new javax.swing.JLabel();
        jTextFieldfacebook = new javax.swing.JTextField();
        jLabelFacebook = new javax.swing.JLabel();
        jLabelTwitter = new javax.swing.JLabel();
        jTextFieldTwitter = new javax.swing.JTextField();
        jTextFieldCep = new javax.swing.JTextField();
        jTextFieldCpf = new javax.swing.JTextField();
        jTextFieldRg = new javax.swing.JTextField();
        jTextFieldTelefoneResidencial = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Clientes ");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 102, 255));
        jLabel1.setText("Apresentação da tela de Cadastro de Clientes ");

        jPanelPrincipal.setBackground(new java.awt.Color(240, 230, 42));
        jPanelPrincipal.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelPrincipal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTableCliente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableCliente);

        jPanelPrincipal.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 216, 928, 168));

        jPanelButon.setBackground(new java.awt.Color(51, 102, 255));
        jPanelButon.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButtonNovo.setFont(new java.awt.Font("Arial Unicode MS", 1, 11)); // NOI18N
        jButtonNovo.setForeground(new java.awt.Color(255, 0, 51));
        jButtonNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Botoes_5122_paper_48.png"))); // NOI18N
        jButtonNovo.setToolTipText("Novo");
        jButtonNovo.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        jButtonSalvar.setFont(new java.awt.Font("Arial Unicode MS", 1, 11)); // NOI18N
        jButtonSalvar.setForeground(new java.awt.Color(255, 0, 51));
        jButtonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/informatica_3629_disquete17.JPG"))); // NOI18N
        jButtonSalvar.setToolTipText("Salvar");
        jButtonSalvar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonSalvar.setEnabled(false);
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonEditar.setFont(new java.awt.Font("Arial Unicode MS", 1, 11)); // NOI18N
        jButtonEditar.setForeground(new java.awt.Color(255, 0, 51));
        jButtonEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Botoes_5121_paperpencil_48.png"))); // NOI18N
        jButtonEditar.setToolTipText("Editar");
        jButtonEditar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonEditar.setEnabled(false);
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonCancelar.setFont(new java.awt.Font("Arial Unicode MS", 1, 11)); // NOI18N
        jButtonCancelar.setForeground(new java.awt.Color(255, 0, 51));
        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Botoes_Site_5750_Knob_Cancel.png"))); // NOI18N
        jButtonCancelar.setToolTipText("Cancelar");
        jButtonCancelar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonCancelar.setEnabled(false);
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonExcluir.setFont(new java.awt.Font("Arial Unicode MS", 1, 11)); // NOI18N
        jButtonExcluir.setForeground(new java.awt.Color(255, 0, 51));
        jButtonExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Botoes_Site_5751_Knob_Remove_Red.png"))); // NOI18N
        jButtonExcluir.setToolTipText("Excluir");
        jButtonExcluir.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonExcluir.setEnabled(false);
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jButtonFechar.setFont(new java.awt.Font("Arial Unicode MS", 1, 11)); // NOI18N
        jButtonFechar.setForeground(new java.awt.Color(255, 0, 51));
        jButtonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/BtnFechar.png"))); // NOI18N
        jButtonFechar.setToolTipText("Fechar");
        jButtonFechar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFecharActionPerformed(evt);
            }
        });

        jButtonPrimeiro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Botoes_5055_arrow_right_48.png"))); // NOI18N
        jButtonPrimeiro.setToolTipText("Seta o primeiro registro");
        jButtonPrimeiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrimeiroActionPerformed(evt);
            }
        });

        jButtonProximo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/2693_32x32botaopng.png"))); // NOI18N
        jButtonProximo.setToolTipText("Seta o próximo registro");
        jButtonProximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonProximoActionPerformed(evt);
            }
        });

        jButtonAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Button_anterior.jpg"))); // NOI18N
        jButtonAnterior.setToolTipText("Seta o registro anterior");
        jButtonAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnteriorActionPerformed(evt);
            }
        });

        jButtonUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Botoes_5053_arrow_left_48.png"))); // NOI18N
        jButtonUltimo.setToolTipText("Seta o último registro");
        jButtonUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUltimoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelButonLayout = new javax.swing.GroupLayout(jPanelButon);
        jPanelButon.setLayout(jPanelButonLayout);
        jPanelButonLayout.setHorizontalGroup(
            jPanelButonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelButonLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelButonLayout.setVerticalGroup(
            jPanelButonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelButonLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelButonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelButonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jButtonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jButtonNovo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonEditar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jButtonProximo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27))
        );

        jPanelPrincipal.add(jPanelButon, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 395, -1, 70));

        jLabelIdCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelIdCliente.setForeground(new java.awt.Color(51, 102, 255));
        jLabelIdCliente.setText("Código do Cliente ");
        jPanelPrincipal.add(jLabelIdCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 13, -1, -1));

        jTextFieldIdCliente.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldIdCliente.setEnabled(false);
        jPanelPrincipal.add(jTextFieldIdCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 34, 113, -1));

        jTextFieldNomeCliente.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldNomeCliente.setEnabled(false);
        jTextFieldNomeCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNomeClienteActionPerformed(evt);
            }
        });
        jPanelPrincipal.add(jTextFieldNomeCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(131, 34, 517, -1));

        jLabelNomeCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelNomeCliente.setForeground(new java.awt.Color(51, 102, 255));
        jLabelNomeCliente.setText("Nome do Cliente ");
        jPanelPrincipal.add(jLabelNomeCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(131, 13, -1, -1));

        jFormattedTextFieldDataEntrada.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jFormattedTextFieldDataEntrada.setEnabled(false);
        jPanelPrincipal.add(jFormattedTextFieldDataEntrada, new org.netbeans.lib.awtextra.AbsoluteConstraints(654, 34, 109, -1));

        jLabelDataEntrada.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelDataEntrada.setForeground(new java.awt.Color(51, 102, 255));
        jLabelDataEntrada.setText("Data de Entrada");
        jPanelPrincipal.add(jLabelDataEntrada, new org.netbeans.lib.awtextra.AbsoluteConstraints(654, 13, -1, -1));

        jFormattedTextFieldDataSaida.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jFormattedTextFieldDataSaida.setEnabled(false);
        jPanelPrincipal.add(jFormattedTextFieldDataSaida, new org.netbeans.lib.awtextra.AbsoluteConstraints(769, 34, 109, -1));

        jLabelDataSaida.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelDataSaida.setForeground(new java.awt.Color(51, 102, 255));
        jLabelDataSaida.setText("Data da Saída");
        jPanelPrincipal.add(jLabelDataSaida, new org.netbeans.lib.awtextra.AbsoluteConstraints(769, 13, -1, -1));

        jTextFieldEndereco.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldEndereco.setEnabled(false);
        jPanelPrincipal.add(jTextFieldEndereco, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 92, 559, -1));

        jLabelEndereco.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelEndereco.setForeground(new java.awt.Color(51, 102, 255));
        jLabelEndereco.setText("Endereço");
        jPanelPrincipal.add(jLabelEndereco, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 71, -1, -1));

        jTextFieldCidade.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldCidade.setEnabled(false);
        jPanelPrincipal.add(jTextFieldCidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(577, 92, 162, -1));

        jLabelCidade.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCidade.setForeground(new java.awt.Color(51, 102, 255));
        jLabelCidade.setText("Cidade");
        jPanelPrincipal.add(jLabelCidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(577, 71, -1, -1));

        jLabelCep.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCep.setForeground(new java.awt.Color(51, 102, 255));
        jLabelCep.setText("CEP");
        jPanelPrincipal.add(jLabelCep, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 121, -1, -1));

        jLabelTelefoneResidencial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelTelefoneResidencial.setForeground(new java.awt.Color(51, 102, 255));
        jLabelTelefoneResidencial.setText("Telefone Residêncial");
        jPanelPrincipal.add(jLabelTelefoneResidencial, new org.netbeans.lib.awtextra.AbsoluteConstraints(357, 121, -1, -1));

        jLabelCelularContato.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCelularContato.setForeground(new java.awt.Color(51, 102, 255));
        jLabelCelularContato.setText("Celular de Contato");
        jPanelPrincipal.add(jLabelCelularContato, new org.netbeans.lib.awtextra.AbsoluteConstraints(509, 121, 123, -1));

        jFormattedTextFieldCelularContato.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jFormattedTextFieldCelularContato.setEnabled(false);
        jPanelPrincipal.add(jFormattedTextFieldCelularContato, new org.netbeans.lib.awtextra.AbsoluteConstraints(509, 142, 123, -1));

        jLabelCpf.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCpf.setForeground(new java.awt.Color(51, 102, 255));
        jLabelCpf.setText("CPF");
        jPanelPrincipal.add(jLabelCpf, new org.netbeans.lib.awtextra.AbsoluteConstraints(127, 121, -1, -1));

        jLabelRg.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelRg.setForeground(new java.awt.Color(51, 102, 255));
        jLabelRg.setText("RG");
        jPanelPrincipal.add(jLabelRg, new org.netbeans.lib.awtextra.AbsoluteConstraints(243, 121, -1, -1));

        jTextFieldEmail.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldEmail.setEnabled(false);
        jPanelPrincipal.add(jTextFieldEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(638, 142, 285, -1));

        jLabelEmail.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelEmail.setForeground(new java.awt.Color(51, 102, 255));
        jLabelEmail.setText("E mail");
        jPanelPrincipal.add(jLabelEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(638, 121, -1, -1));

        jTextFieldfacebook.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldfacebook.setEnabled(false);
        jPanelPrincipal.add(jTextFieldfacebook, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 187, 385, -1));

        jLabelFacebook.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelFacebook.setForeground(new java.awt.Color(51, 102, 255));
        jLabelFacebook.setText("Facebook");
        jPanelPrincipal.add(jLabelFacebook, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 166, -1, -1));

        jLabelTwitter.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelTwitter.setForeground(new java.awt.Color(51, 102, 255));
        jLabelTwitter.setText("Twitter");
        jPanelPrincipal.add(jLabelTwitter, new org.netbeans.lib.awtextra.AbsoluteConstraints(403, 166, -1, -1));

        jTextFieldTwitter.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldTwitter.setEnabled(false);
        jPanelPrincipal.add(jTextFieldTwitter, new org.netbeans.lib.awtextra.AbsoluteConstraints(403, 187, 520, -1));

        jTextFieldCep.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldCep.setEnabled(false);
        jPanelPrincipal.add(jTextFieldCep, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 142, 109, -1));

        jTextFieldCpf.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldCpf.setEnabled(false);
        jPanelPrincipal.add(jTextFieldCpf, new org.netbeans.lib.awtextra.AbsoluteConstraints(127, 142, 109, -1));

        jTextFieldRg.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldRg.setEnabled(false);
        jPanelPrincipal.add(jTextFieldRg, new org.netbeans.lib.awtextra.AbsoluteConstraints(242, 142, 109, -1));

        jTextFieldTelefoneResidencial.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldTelefoneResidencial.setEnabled(false);
        jPanelPrincipal.add(jTextFieldTelefoneResidencial, new org.netbeans.lib.awtextra.AbsoluteConstraints(357, 142, 146, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(262, 262, 262)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setSize(new java.awt.Dimension(991, 590));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
        // :

        jTextFieldNomeCliente.setText("");
        jFormattedTextFieldDataEntrada.setText("");
        jFormattedTextFieldDataSaida.setText("");
        jTextFieldEndereco.setText("");
        jTextFieldCidade.setText("");
        
        jTextFieldCep.setText("");
        jTextFieldCpf.setText("");
        jTextFieldRg.setText("");
        jTextFieldTelefoneResidencial.setText("");
        jFormattedTextFieldCelularContato.setText("");
        jTextFieldEmail.setText("");
        jTextFieldfacebook.setText("");
        jTextFieldTwitter.setText("");

        // Declaração do código para Habiitar e desabilitar os botões após cricar em novo:
           jButtonCancelar.setEnabled(true);
           jButtonEditar.setEnabled(false);
           jButtonExcluir.setEnabled(false);
           jButtonSalvar.setEnabled(true);

            jTextFieldNomeCliente.setEnabled(true);
            jTextFieldNomeCliente.requestFocus();
            jFormattedTextFieldDataEntrada.setEnabled(true);
            jFormattedTextFieldDataSaida.setEnabled(true);
            jTextFieldEndereco.setEnabled(true);
            jTextFieldCidade.setEnabled(true);
            
            jTextFieldCep.setEnabled(true);
            jTextFieldCpf.setEnabled(true);
            jTextFieldRg.setEnabled(true);
            jTextFieldTelefoneResidencial.setEnabled(true); 
            jFormattedTextFieldCelularContato.setEnabled(true);
            jTextFieldEmail.setEnabled(true); 
            jTextFieldfacebook.setEnabled(true);
            jTextFieldTwitter.setEnabled(true);

             jButtonNovo.setEnabled(false);
    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        // :

        mod.setNomeCliente(jTextFieldNomeCliente.getText());
        mod.setDataEntrada(jFormattedTextFieldDataEntrada.getText());
        mod.setDataSaida(jFormattedTextFieldDataSaida.getText());
        mod.setEndereco(jTextFieldEndereco.getText());
        mod.setCidade(jTextFieldCidade.getText());
        
        mod.setCep(jTextFieldCep.getText());
        mod.setCpf(jTextFieldCpf.getText());
        mod.setRg(jTextFieldRg.getText());
        mod.setTelefoneResidencial(jTextFieldTelefoneResidencial.getText());
        mod.setCelularContato(jFormattedTextFieldCelularContato.getText());
        mod.setEmail(jTextFieldEmail.getText());
        mod.setFacebook(jTextFieldfacebook.getText());
        mod.setTwitter(jTextFieldTwitter.getText());

        ControleCadastroCliente cliente = new ControleCadastroCliente();
        cliente.SalvarDados(mod);
        preencherTabela("SELECT * FROM cliente  ORDER BY Id_Cliente");

        jTextFieldNomeCliente.setText("");
        jFormattedTextFieldDataEntrada.setText("");
        jFormattedTextFieldDataSaida.setText("");
        jTextFieldEndereco.setText("");
        jTextFieldCidade.setText("");
        
        jTextFieldCep.setText("");
        jTextFieldCpf.setText("");
        jTextFieldRg.setText("");
        jTextFieldTelefoneResidencial.setText("");
        jFormattedTextFieldCelularContato.setText("");
        jTextFieldEmail.setText("");
        jTextFieldfacebook.setText("");
        jTextFieldTwitter.setText("");

        jTextFieldNomeCliente.setEnabled(!true);
        jFormattedTextFieldDataEntrada.setEnabled(!true);
        jFormattedTextFieldDataSaida.setEnabled(!true);
        jTextFieldEndereco.setEnabled(!true);
        jTextFieldCidade.setEnabled(!true);
        
        jTextFieldCep.setEnabled(!true);
        jTextFieldCpf.setEnabled(!true);
        jTextFieldRg.setEnabled(!true);
        jTextFieldTelefoneResidencial.setEnabled(!true);
        jFormattedTextFieldCelularContato.setEnabled(!true);
        jTextFieldEmail.setEnabled(!true);
        jTextFieldfacebook.setEnabled(!true);
        jTextFieldTwitter.setEnabled(!true);

        // Declaração do código para Habiitar e desabilitar os botões após cricar em cancelar:
        jButtonCancelar.setEnabled(false);
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvar.setEnabled(true);
        jButtonNovo.setEnabled(true);
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        // Declaração do botão editar do Form Cadastro de Usuário:

        mod.setNomeCliente(jTextFieldNomeCliente.getText());
        mod.setDataEntrada(jFormattedTextFieldDataEntrada.getText());
        mod.setDataSaida(jFormattedTextFieldDataSaida.getText());
        mod.setEndereco(jTextFieldEndereco.getText());
        mod.setCidade(jTextFieldCidade.getText());
        
        mod.setCep(jTextFieldCep.getText());
        mod.setCpf(jTextFieldCpf.getText());
        mod.setRg(jTextFieldRg.getText());
        mod.setTelefoneResidencial(jTextFieldTelefoneResidencial.getText());
        mod.setCelularContato(jFormattedTextFieldCelularContato.getText());
        mod.setEmail(jTextFieldEmail.getText());
        mod.setFacebook(jTextFieldfacebook.getText());
        mod.setTwitter(jTextFieldTwitter.getText());
        mod.setIdCliente(Integer.parseInt(jTextFieldIdCliente.getText()));

        ControleCadastroCliente  cliente = new ControleCadastroCliente();
        cliente.EditarDados(mod);

        preencherTabela("SELECT * FROM cliente  ORDER BY Id_Cliente");
        
         jTextFieldNomeCliente.setText("");
        jFormattedTextFieldDataEntrada.setText("");
        jFormattedTextFieldDataSaida.setText("");
        jTextFieldEndereco.setText("");
        jTextFieldCidade.setText("");
        
        jTextFieldCep.setText("");
        jTextFieldCpf.setText("");
        jTextFieldRg.setText("");
        jTextFieldTelefoneResidencial.setText("");
        jFormattedTextFieldCelularContato.setText("");
        jTextFieldEmail.setText("");
        jTextFieldfacebook.setText("");
        jTextFieldTwitter.setText("");

        jTextFieldNomeCliente.setEnabled(!true);
        jFormattedTextFieldDataEntrada.setEnabled(!true);
        jFormattedTextFieldDataSaida.setEnabled(!true);
        jTextFieldEndereco.setEnabled(!true);
        jTextFieldCidade.setEnabled(!true);
        
        jTextFieldCep.setEnabled(!true);
        jTextFieldCpf.setEnabled(!true);
        jTextFieldRg.setEnabled(!true);
        jTextFieldTelefoneResidencial.setEnabled(!true);
        jFormattedTextFieldCelularContato.setEnabled(!true);
        jTextFieldEmail.setEnabled(!true);
        jTextFieldfacebook.setEnabled(!true);
        jTextFieldTwitter.setEnabled(!true);

        // Declaração do código para Habiitar e desabilitar os botões após cricar em cancelar:
        jButtonCancelar.setEnabled(false);
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvar.setEnabled(true);
        jButtonNovo.setEnabled(true);
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        // Declaração do botão cancelar do Form Cadastro de Usuário:

        jTextFieldNomeCliente.setText("");
        jFormattedTextFieldDataEntrada.setText("");
        jFormattedTextFieldDataSaida.setText("");
        jTextFieldEndereco.setText("");
        jTextFieldCidade.setText("");
        
        jTextFieldCep.setText("");
        jTextFieldCpf.setText("");
        jTextFieldRg.setText("");
        jTextFieldTelefoneResidencial.setText("");
        jFormattedTextFieldCelularContato.setText("");
        jTextFieldEmail.setText("");
        jTextFieldfacebook.setText("");
        jTextFieldTwitter.setText("");

        jTextFieldNomeCliente.setEnabled(!true);
        jFormattedTextFieldDataEntrada.setEnabled(!true);
        jFormattedTextFieldDataSaida.setEnabled(!true);
        jTextFieldEndereco.setEnabled(!true);
        jTextFieldCidade.setEnabled(!true);
        
        jTextFieldCep.setEnabled(!true);
        jTextFieldCpf.setEnabled(!true);
        jTextFieldRg.setEnabled(!true);
        jTextFieldTelefoneResidencial.setEnabled(!true);
        jFormattedTextFieldCelularContato.setEnabled(!true);
        jTextFieldEmail.setEnabled(!true);
        jTextFieldfacebook.setEnabled(!true);
        jTextFieldTwitter.setEnabled(!true);

        // Declaração do código para Habiitar e desabilitar os botões após cricar em cancelar:
        jButtonCancelar.setEnabled(false);
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvar.setEnabled(true);
        jButtonNovo.setEnabled(true);
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
        // Declaração do botão excluir do Form Cadastro de Usuário:

        mod.setIdCliente(Integer.parseInt(jTextFieldIdCliente.getText()));
        mod.setNomeCliente(jTextFieldNomeCliente.getText());
        mod.setDataEntrada(jFormattedTextFieldDataEntrada.getText());
        mod.setDataSaida(jFormattedTextFieldDataSaida.getText());
        mod.setEndereco(jTextFieldEndereco.getText());
        mod.setCidade(jTextFieldCidade.getText());
        
        mod.setCep(jTextFieldCep.getText());
        mod.setCpf(jTextFieldCpf.getText());
        mod.setRg(jTextFieldRg.getText());
        mod.setTelefoneResidencial(jTextFieldTelefoneResidencial.getText());
        mod.setCelularContato(jFormattedTextFieldCelularContato.getText());
        mod.setEmail(jTextFieldEmail.getText());
        mod.setFacebook(jTextFieldfacebook.getText());
        mod.setTwitter(jTextFieldTwitter.getText());

        ControleCadastroCliente  cliente = new ControleCadastroCliente();
        cliente.ExcluirDados(mod);

       preencherTabela("SELECT * FROM cliente  ORDER BY Id_Cliente");

        jTextFieldNomeCliente.setText("");
        jFormattedTextFieldDataEntrada.setText("");
        jFormattedTextFieldDataSaida.setText("");
        jTextFieldEndereco.setText("");
        jTextFieldCidade.setText("");
        
        jTextFieldCep.setText("");
        jTextFieldCpf.setText("");
        jTextFieldRg.setText("");
        jTextFieldTelefoneResidencial.setText("");
        jFormattedTextFieldCelularContato.setText("");
        jTextFieldEmail.setText("");
        jTextFieldfacebook.setText("");
        jTextFieldTwitter.setText("");

        jTextFieldNomeCliente.setEnabled(!true);
        jFormattedTextFieldDataEntrada.setEnabled(!true);
        jFormattedTextFieldDataSaida.setEnabled(!true);
        jTextFieldEndereco.setEnabled(!true);
        jTextFieldCidade.setEnabled(!true);
        
        jTextFieldCep.setEnabled(!true);
        jTextFieldCpf.setEnabled(!true);
        jTextFieldRg.setEnabled(!true);
        jTextFieldTelefoneResidencial.setEnabled(!true);
        jFormattedTextFieldCelularContato.setEnabled(!true);
        jTextFieldEmail.setEnabled(!true);
        jTextFieldfacebook.setEnabled(!true);
        jTextFieldTwitter.setEnabled(!true);

        // Declaração do código para Habiitar e desabilitar os botões após cricar em cancelar:
        jButtonCancelar.setEnabled(false);
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvar.setEnabled(true);
        jButtonNovo.setEnabled(true);
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jButtonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFecharActionPerformed
        // Declaração do  Código do botão fechar do Form Cadastro de Usuário:
        dispose();
        JOptionPane.showMessageDialog(null, "Deseja realmente sair do Cadastro de Cliente?");
    }//GEN-LAST:event_jButtonFecharActionPerformed

    private void jButtonPrimeiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrimeiroActionPerformed
        // Declaração do botão primeiro referente ao botão editar do Form Cadastro de Usuário:

        // Declaração do código para Habiitar e desabilitar os botões após cricar em novo:
        jButtonCancelar.setEnabled(true);
        jButtonEditar.setEnabled(true);
        jButtonExcluir.setEnabled(true);
        jButtonSalvar.setEnabled(false);
        jButtonNovo.setEnabled(false);

        jTextFieldNomeCliente.setEnabled(true);
        jTextFieldNomeCliente.requestFocus();
        jFormattedTextFieldDataEntrada.setEnabled(true);
        jFormattedTextFieldDataSaida.setEnabled(true);
        jTextFieldEndereco.setEnabled(true);
        jTextFieldCidade.setEnabled(true);
       
        jTextFieldCep.setEnabled(true);
        jTextFieldCpf.setEnabled(true);
        jTextFieldRg.setEnabled(true);
        jTextFieldTelefoneResidencial.setEnabled(true); 
        jFormattedTextFieldCelularContato.setEnabled(true);
        jTextFieldEmail.setEnabled(true); 
        jTextFieldfacebook.setEnabled(true);
        jTextFieldTwitter.setEnabled(true);

        try {
            conectar.executaSQL("SELECT * FROM cliente ORDER BY Id_Cliente");
            conectar.rs.first();

            jTextFieldIdCliente.setText(String.valueOf(conectar.rs.getInt("Id_Cliente")));
            jTextFieldNomeCliente.setText(conectar.rs.getString("Nome_Cliente"));
            jFormattedTextFieldDataEntrada.setText(conectar.rs.getString("Data_Entrada"));
            jFormattedTextFieldDataSaida.setText(conectar.rs.getString("Data_Saida"));
            jTextFieldEndereco.setText(conectar.rs.getString("Endereco_Cliente"));
            jTextFieldCidade.setText(conectar.rs.getString("Cidade"));
           
            jTextFieldCep.setText(conectar.rs.getString("Cep"));
            jTextFieldCpf.setText(conectar.rs.getString("Cpf"));
            jTextFieldRg.setText(conectar.rs.getString("Rg"));
            jTextFieldTelefoneResidencial.setText(conectar.rs.getString("Telefone_Residencial"));
            jFormattedTextFieldCelularContato.setText(conectar.rs.getString("Celular_Contato"));
            jTextFieldEmail.setText(conectar.rs.getString("Email"));
            jTextFieldfacebook.setText(conectar.rs.getString("Facebook"));
            jTextFieldTwitter.setText(conectar.rs.getString("Twitter"));

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao setar o primeiro registro! \n Erro: " + ex);
        }

    }//GEN-LAST:event_jButtonPrimeiroActionPerformed

    private void jButtonProximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonProximoActionPerformed
        // Declaração do botão primeiro referente ao botão editar do Form Cadastro de Usuário:

        // Declaração do código para Habiitar e desabilitar os botões após cricar em novo:
        jButtonCancelar.setEnabled(true);
        jButtonEditar.setEnabled(true);
        jButtonExcluir.setEnabled(true);
        jButtonSalvar.setEnabled(false);
        jButtonNovo.setEnabled(false);

        jTextFieldNomeCliente.setEnabled(true);
        jTextFieldNomeCliente.requestFocus();
        jFormattedTextFieldDataEntrada.setEnabled(true);
        jFormattedTextFieldDataSaida.setEnabled(true);
        jTextFieldEndereco.setEnabled(true);
        jTextFieldCidade.setEnabled(true);
       
        jTextFieldCep.setEnabled(true);
        jTextFieldCpf.setEnabled(true);
        jTextFieldRg.setEnabled(true);
        jTextFieldTelefoneResidencial.setEnabled(true); 
        jFormattedTextFieldCelularContato.setEnabled(true);
        jTextFieldEmail.setEnabled(true); 
        jTextFieldfacebook.setEnabled(true);
        jTextFieldTwitter.setEnabled(true);

        try {
            
            conectar.rs.next();

            jTextFieldIdCliente.setText(String.valueOf(conectar.rs.getInt("Id_Cliente")));
            jTextFieldNomeCliente.setText(conectar.rs.getString("Nome_Cliente"));
            jFormattedTextFieldDataEntrada.setText(conectar.rs.getString("Data_Entrada"));
            jFormattedTextFieldDataSaida.setText(conectar.rs.getString("Data_Saida"));
            jTextFieldEndereco.setText(conectar.rs.getString("Endereco_Cliente"));
            jTextFieldCidade.setText(conectar.rs.getString("Cidade"));
            
            jTextFieldCep.setText(conectar.rs.getString("Cep"));
            jTextFieldCpf.setText(conectar.rs.getString("Cpf"));
            jTextFieldRg.setText(conectar.rs.getString("Rg"));
            jTextFieldTelefoneResidencial.setText(conectar.rs.getString("Telefone_Residencial"));
            jFormattedTextFieldCelularContato.setText(conectar.rs.getString("Celular_Contato"));
            jTextFieldEmail.setText(conectar.rs.getString("Email"));
            jTextFieldfacebook.setText(conectar.rs.getString("Facebook"));
            jTextFieldTwitter.setText(conectar.rs.getString("Twitter"));

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao setar o próximo registro! \n Erro: " + ex);
        }
    }//GEN-LAST:event_jButtonProximoActionPerformed

    private void jButtonAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnteriorActionPerformed
        // Declaração do botão primeiro referente ao botão editar do Form Cadastro de Usuário:

        // Declaração do código para Habiitar e desabilitar os botões após cricar em novo:
        jButtonCancelar.setEnabled(true);
        jButtonEditar.setEnabled(true);
        jButtonExcluir.setEnabled(true);
        jButtonSalvar.setEnabled(false);
        jButtonNovo.setEnabled(false);

        jTextFieldNomeCliente.setEnabled(true);
        jTextFieldNomeCliente.requestFocus();
        jFormattedTextFieldDataEntrada.setEnabled(true);
        jFormattedTextFieldDataSaida.setEnabled(true);
        jTextFieldEndereco.setEnabled(true);
        jTextFieldCidade.setEnabled(true);
        
        jTextFieldCep.setEnabled(true);
        jTextFieldCpf.setEnabled(true);
        jTextFieldRg.setEnabled(true);
        jTextFieldTelefoneResidencial.setEnabled(true); 
        jFormattedTextFieldCelularContato.setEnabled(true);
        jTextFieldEmail.setEnabled(true); 
        jTextFieldfacebook.setEnabled(true);
        jTextFieldTwitter.setEnabled(true);

        try {
           
            conectar.rs.previous();

            jTextFieldIdCliente.setText(String.valueOf(conectar.rs.getInt("Id_Cliente")));
            jTextFieldNomeCliente.setText(conectar.rs.getString("Nome_Cliente"));
            jFormattedTextFieldDataEntrada.setText(conectar.rs.getString("Data_Entrada"));
            jFormattedTextFieldDataSaida.setText(conectar.rs.getString("Data_Saida"));
            jTextFieldEndereco.setText(conectar.rs.getString("Endereco_Cliente"));
            jTextFieldCidade.setText(conectar.rs.getString("Cidade"));
            
            jTextFieldCep.setText(conectar.rs.getString("Cep"));
            jTextFieldCpf.setText(conectar.rs.getString("Cpf"));
            jTextFieldRg.setText(conectar.rs.getString("Rg"));
            jTextFieldTelefoneResidencial.setText(conectar.rs.getString("Telefone_Residencial"));
            jFormattedTextFieldCelularContato.setText(conectar.rs.getString("Celular_Contato"));
            jTextFieldEmail.setText(conectar.rs.getString("Email"));
            jTextFieldfacebook.setText(conectar.rs.getString("Facebook"));
            jTextFieldTwitter.setText(conectar.rs.getString("Twitter"));

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao setar o registro anterior! \n Erro: " + ex);
        }
    }//GEN-LAST:event_jButtonAnteriorActionPerformed

    private void jButtonUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUltimoActionPerformed
        // Declaração do botão primeiro referente ao botão editar do Form Cadastro de Usuário:

        // Declaração do código para Habiitar e desabilitar os botões após cricar em novo:
        jButtonCancelar.setEnabled(true);
        jButtonEditar.setEnabled(true);
        jButtonExcluir.setEnabled(true);
        jButtonSalvar.setEnabled(false);
        jButtonNovo.setEnabled(false);

        jTextFieldNomeCliente.setEnabled(true);
        jTextFieldNomeCliente.requestFocus();
        jFormattedTextFieldDataEntrada.setEnabled(true);
        jFormattedTextFieldDataSaida.setEnabled(true);
        jTextFieldEndereco.setEnabled(true);
        jTextFieldCidade.setEnabled(true);
        
        jTextFieldCep.setEnabled(true);
        jTextFieldCpf.setEnabled(true);
        jTextFieldRg.setEnabled(true);
        jTextFieldTelefoneResidencial.setEnabled(true); 
        jFormattedTextFieldCelularContato.setEnabled(true);
        jTextFieldEmail.setEnabled(true); 
        jTextFieldfacebook.setEnabled(true);
        jTextFieldTwitter.setEnabled(true);

        try {
            conectar.executaSQL("SELECT * FROM cliente ORDER BY Id_Cliente");
            conectar.rs.last();

            jTextFieldIdCliente.setText(String.valueOf(conectar.rs.getInt("Id_Cliente")));
            jTextFieldNomeCliente.setText(conectar.rs.getString("Nome_Cliente"));
            jFormattedTextFieldDataEntrada.setText(conectar.rs.getString("Data_Entrada"));
            jFormattedTextFieldDataSaida.setText(conectar.rs.getString("Data_Saida"));
            jTextFieldEndereco.setText(conectar.rs.getString("Endereco_Cliente"));
            jTextFieldCidade.setText(conectar.rs.getString("Cidade"));
            
            jTextFieldCep.setText(conectar.rs.getString("Cep"));
            jTextFieldCpf.setText(conectar.rs.getString("Cpf"));
            jTextFieldRg.setText(conectar.rs.getString("Rg"));
            jTextFieldTelefoneResidencial.setText(conectar.rs.getString("Telefone_Residencial"));
            jFormattedTextFieldCelularContato.setText(conectar.rs.getString("Celular_Contato"));
            jTextFieldEmail.setText(conectar.rs.getString("Email"));
            jTextFieldfacebook.setText(conectar.rs.getString("Facebook"));
            jTextFieldTwitter.setText(conectar.rs.getString("Twitter"));

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao setar o último registro! \n Erro: " + ex);
        }
    }//GEN-LAST:event_jButtonUltimoActionPerformed

    private void jTextFieldNomeClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNomeClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNomeClienteActionPerformed

    public void preencherTabela(String SQL){
   
         ArrayList dados = new ArrayList();
    
          String [] Colunas = new String [] { "Id_Cliente", "Nome do Cliente", "Data de Entrada", "Data da Saida",
          "Endereço", "Cidade", "Estado", "Cep", "Cpf", "Rg", "Telefone Residêncial", "Celular de Contato",
          "E-mail", "Facebook", "Twitter" };
                     
             try {
                 conectar.executaSQL(SQL);
                 conectar.rs.first();
                  do{
                    dados.add(new Object[]{conectar.rs.getInt("Id_Cliente"),
                    conectar.rs.getString("Nome_Cliente"), conectar.rs.getString("Data_Entrada"), 
                    conectar.rs.getString("Data_Saida"),conectar.rs.getString("Endereco_Cliente"),
                    conectar.rs.getString("Cidade"), conectar.rs.getString("Estado"), 
                    conectar.rs.getString("Cep"), conectar.rs.getString("Cpf"), 
                    conectar.rs.getString("Rg"), conectar.rs.getString("Telefone_Residencial"),
                    conectar.rs.getString("Celular_Contato"), conectar.rs.getString("Email"),
                    conectar.rs.getString("Facebook"), conectar.rs.getString("Twitter")});
                 }while(conectar.rs.next());   
             } catch (Exception ex) {
                 JOptionPane.showMessageDialog(null, "Erro ao preencher o ArrayList!\n Erro: " + ex);
             }
             
                ModeloTabela modelo = new ModeloTabela(dados, Colunas);
               jTableCliente.setModel(modelo);
               jTableCliente.getColumnModel().getColumn(0).setPreferredWidth(130);
               jTableCliente.getColumnModel().getColumn(0).setResizable(false);
               jTableCliente.getColumnModel().getColumn(1).setPreferredWidth(180);
               jTableCliente.getColumnModel().getColumn(1).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(2).setPreferredWidth(100);
               jTableCliente.getColumnModel().getColumn(2).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(3).setPreferredWidth(130);
               jTableCliente.getColumnModel().getColumn(3).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(4).setPreferredWidth(180);
               jTableCliente.getColumnModel().getColumn(4).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(5).setPreferredWidth(100);
               jTableCliente.getColumnModel().getColumn(5).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(6).setPreferredWidth(100);
               jTableCliente.getColumnModel().getColumn(6).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(7).setPreferredWidth(120);
               jTableCliente.getColumnModel().getColumn(7).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(8).setPreferredWidth(120);
               jTableCliente.getColumnModel().getColumn(8).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(9).setPreferredWidth(120);
               jTableCliente.getColumnModel().getColumn(9).setResizable(false);
               jTableCliente.getColumnModel().getColumn(10).setPreferredWidth(100);
               jTableCliente.getColumnModel().getColumn(10).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(11).setPreferredWidth(100);
               jTableCliente.getColumnModel().getColumn(11).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(12).setPreferredWidth(200);
               jTableCliente.getColumnModel().getColumn(12).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(13).setPreferredWidth(200);
               jTableCliente.getColumnModel().getColumn(13).setResizable(false); 
               jTableCliente.getColumnModel().getColumn(14).setPreferredWidth(200);
               jTableCliente.getColumnModel().getColumn(14).setResizable(false);
               jTableCliente.getTableHeader().setReorderingAllowed(false);
               jTableCliente.setAutoResizeMode( jTableCliente.AUTO_RESIZE_OFF);
               jTableCliente.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                  
        }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FrmCadastroCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAnterior;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonFechar;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPrimeiro;
    private javax.swing.JButton jButtonProximo;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonUltimo;
    private javax.swing.JFormattedTextField jFormattedTextFieldCelularContato;
    private javax.swing.JFormattedTextField jFormattedTextFieldDataEntrada;
    private javax.swing.JFormattedTextField jFormattedTextFieldDataSaida;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelCelularContato;
    private javax.swing.JLabel jLabelCep;
    private javax.swing.JLabel jLabelCidade;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelDataEntrada;
    private javax.swing.JLabel jLabelDataSaida;
    private javax.swing.JLabel jLabelEmail;
    private javax.swing.JLabel jLabelEndereco;
    private javax.swing.JLabel jLabelFacebook;
    private javax.swing.JLabel jLabelIdCliente;
    private javax.swing.JLabel jLabelNomeCliente;
    private javax.swing.JLabel jLabelRg;
    private javax.swing.JLabel jLabelTelefoneResidencial;
    private javax.swing.JLabel jLabelTwitter;
    private javax.swing.JPanel jPanelButon;
    private javax.swing.JPanel jPanelPrincipal;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableCliente;
    private javax.swing.JTextField jTextFieldCep;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldEmail;
    private javax.swing.JTextField jTextFieldEndereco;
    private javax.swing.JTextField jTextFieldIdCliente;
    private javax.swing.JTextField jTextFieldNomeCliente;
    private javax.swing.JTextField jTextFieldRg;
    private javax.swing.JTextField jTextFieldTelefoneResidencial;
    private javax.swing.JTextField jTextFieldTwitter;
    private javax.swing.JTextField jTextFieldfacebook;
    // End of variables declaration//GEN-END:variables
}
