
package Model;

/**
 *
 * @author: José Walter.
 */
public class ModeloVenda {
    
    private int    Idvenda;
    private String DataVenda;
    private float  ValorVenda;
    private String NomeCliente;
    private String NomeProduto;
    private int QuantidadeItem;

    /**
     * @return the Idvenda
     */
    public int getIdvenda() {
        return Idvenda;
    }

    /**
     * @param Idvenda the Idvenda to set
     */
    public void setIdvenda(int Idvenda) {
        this.Idvenda = Idvenda;
    }

    /**
     * @return the DataVenda
     */
    public String getDataVenda() {
        return DataVenda;
    }

    /**
     * @param DataVenda the DataVenda to set
     */
    public void setDataVenda(String DataVenda) {
        this.DataVenda = DataVenda;
    }

    /**
     * @return the ValorVenda
     */
    public float getValorVenda() {
        return ValorVenda;
    }

    /**
     * @param ValorVenda the ValorVenda to set
     */
    public void setValorVenda(float ValorVenda) {
        this.ValorVenda = ValorVenda;
    }

    /**
     * @return the NomeCliente
     */
    public String getNomeCliente() {
        return NomeCliente;
    }

    /**
     * @param NomeCliente the NomeCliente to set
     */
    public void setNomeCliente(String NomeCliente) {
        this.NomeCliente = NomeCliente;
    }

    /**
     * @return the NomeProduto
     */
    public String getNomeProduto() {
        return NomeProduto;
    }

    /**
     * @param NomeProduto the NomeProduto to set
     */
    public void setNomeProduto(String NomeProduto) {
        this.NomeProduto = NomeProduto;
    }

    /**
     * @return the QuantidadeItem
     */
    public int getQuantidadeItem() {
        return QuantidadeItem;
    }

    /**
     * @param QuantidadeItem the QuantidadeItem to set
     */
    public void setQuantidadeItem(int QuantidadeItem) {
        this.QuantidadeItem = QuantidadeItem;
    }

    public void getQuantidadeItem(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
}
